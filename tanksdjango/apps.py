from django.apps import AppConfig


class TanksdjangoConfig(AppConfig):
    name = 'tanksdjango'
