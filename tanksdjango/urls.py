from django.conf.urls import url

from tanksdjango import views

urlpatterns = [
    url(r'^$', views.HomePageView.as_view()),
    url(r'^db/', views.DataBaseView.as_view()),
]
