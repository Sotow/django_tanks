from django.db import models

class TanksImage(models.Model):
    id = models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')
    site = models.CharField(max_length=10000)
    url = models.CharField(max_length=10000)

    class Meta:
        db_table = 't_tanks_images'
        managed = False

    def __str__(self):
        return self.site

class TanksModel(models.Model):
    name = models.CharField(max_length=255)
    image = models.ForeignKey(TanksImage)

    class Meta:
        db_table = 't_tanks'
        managed = False

    def __str__(self):
        return self.name