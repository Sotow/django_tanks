from django.shortcuts import render, render_to_response
from django.views.generic import TemplateView

from tanksdjango.models import TanksModel


# Create your views here.
class HomePageView(TemplateView):
    def get(self, request, **kwargs):
        return render(request, 'index.html', context=None)

class DataBaseView(TemplateView):
    def get(self, request, **kwargs):
        tank = TanksModel.objects.filter(name='T-34')  # todo add filtering
        return render_to_response('databased.html', {'tank': tank[0]})